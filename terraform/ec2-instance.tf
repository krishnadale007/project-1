terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}
provider "aws" {
    region = "var.region"
    shared_config_files = ["/home/ubuntu/.aws/config"]
  shared_credentials_files = ["/home/ubuntu/.aws/credentials"]
 profile = "var.profile"
}
resource "aws_instance" "my_instance" {
    ami = var.ami
instance_type = var.instance_type
key_name =   var.key_name
tags = var.tags 
}

variable "region" {
    type = string
    default = "us-east-1"
    description = "this is region"
  }
  variable "profile" {
    type = string
    default = "krishna"
    description = "this is profile of user"
    }
    variable "ami" {
        type = string
        default = "ami-0fe630eb857a6ec83"
        description = "this is ami for redhat"
      }
      variable "instance_type" {
        type = string
        default = "t2.micro"
        description = "this is instance of t2.micro"
     }
     variable "key_name" {
        type = string
        default = "virginia"
       description = "this is key name "
     }
     variable "tags" {
        type = map
       description = "this is tags "
       default = {
        Name =  "Redhat-server"
        enviroment = "staging"
        project = "AA"
       }
       output "created_server" {
        value ="yes"
        }
        }