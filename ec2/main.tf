terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.41.0"
    }
  }
}
# i have commited that region for use ec2 servre roles
provider "aws" {
   region = var.region
#   shared_config_files      = ["/home/ubuntu/.aws/config"]
#   shared_credentials_files = ["/home/ubuntu/.aws/credentials"]
#   profile                  = var.profile
}
 terraform {
 backend "s3" {
   bucket         = "prod-terraform.tfstate-b20"
   key            = "terraform.tfstate"
   region         = "us-east-1"
   encrypt        = true
   dynamodb_table = "terraform-b20"
 }
}
  resource "aws_instance" "my_instance" {
    ami = var.ami
    instance_type = var.instance_type
    key_name = var.key_name
    tags =var.tags
    
  }

