variable "region" {
    type = string
    description = "here i am providing region for my script"
  }

  variable "profile" {
    type = string
    description = "here i am providing profile for my script"
  }

  variable "ami" {
    type = string
    description = "here i am providing ami for my instance"
  }

  variable "instance_type" {
    type = string
    description = "here i am providing  for instance type"
  }

  variable "key_name" {
    type = string
    description = "here i am providing key for my instance"
  }

variable "tags" {
    type = map
    description = "here i am providing tag for my instance"
  }
  
  